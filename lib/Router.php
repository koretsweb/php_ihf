<?php

abstract class Router
{
    /**
     * @param string $to
     */
    public static function redirect($to)
    {
        header('Location:' . $to);
    }
}