<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="processor.php" method="post">
    <label for="username">Username</label>
    <input type="text" name="username">

    <label for="email">Email</label>
    <input type="email" name="email">

    <label for="password">Password</label>
    <input type="password" name="password">

    <label for="adult">18?</label>
    <input type="checkbox" name="adult">

    <button type="submit">save</button>
</form>
</body>
</html>