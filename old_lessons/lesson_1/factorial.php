<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 26.03.18
 */

function getFactorial($num, $value = 1, $first = true)
{

    if ($num === 1) {
        $file = fopen('file.txt', 'r+');
        fwrite($file, $value, 2);
        fclose($file);
        print_r($value);
        return;
    }

    if ($first) {
        $value *= $num * ($num - 1); // 20
    } else {
        $value *= $num -1;
    }
    getFactorial(--$num, $value, false);
}