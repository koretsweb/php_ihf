<?php
class Figure
{
    public function getSquare()
    {

    }
}

class Circle extends Figure
{
    const PI = 3.14;

    public $radius;

    public function getSquare()
    {
        return self::PI * pow($this->radius, 2);
    }
}

class Rectangle extends Figure
{
    public $height;

    public $width;

    public function getSquare()
    {
        return $this->height * $this->width;
    }
}

$circle = new Circle();
$circle->radius = 5;
echo $circle->getSquare();

$rectangle = new Rectangle());
$rectangle->width = 5;
$rectangle->height= 15;
echo $rectangle->getSquare();
