<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 10.04.18
 */
$file = $_FILES['file'];

$data = json_decode(file_get_contents($file['tmp_name']));

$max = $data[0];

foreach ($data as $object) {
    if ($object->cpv > $max->cpv) {
        $max = $object;
    }
}

echo "Name: {$max->name} <br> Year: {$max->year} <br> Sum: {$max->cpv}";
